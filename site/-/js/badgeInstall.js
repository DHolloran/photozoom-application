var flashvars = {};
flashvars.id = 'air_badge';
flashvars.airversion = '2.6';
flashvars.appname = 'PhotoZoom';
flashvars.appurl = 'https://s3.amazonaws.com/DKdesigns_images/photo_zoom/PhotoZoom.air';
flashvars.imageurl = 'https://s3.amazonaws.com/DKdesigns_images/photo_zoom/PhotoZoom_205x170.png';
flashvars.appid = 'com.holloran.PhotoZoom';
flashvars.pubid = '';
flashvars.appversion = '1.0';
flashvars.installarg = 'null';
flashvars.launcharg = 'null';
flashvars.helpurl = 'help.html';
flashvars.hidehelp = 'true';
flashvars.skiptransition = 'false';
flashvars.titlecolor = '#aaff';
flashvars.buttonlabelcolor = '#aaff';
flashvars.appnamecolor = '#aaff';


var params = {};
params.wmode = 'window';
params.menu = 'false';
params.quality = 'high';

var attributes = {};

swfobject.embedSWF('-/install_swf/AIRInstallBadge.swf', 'badge_div', '215', '180', '9.0.115', '-/install_swf/expressInstall.swf', flashvars, params, attributes);