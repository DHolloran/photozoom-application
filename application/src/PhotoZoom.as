package
{
    import com.holloran.App;
    import flash.display.NativeWindow;
    import flash.display.Sprite;
    import flash.system.Capabilities;

    [SWF(width = "1200", height = "675")]
    public class PhotoZoom extends Sprite
    {
        public function PhotoZoom()
        {
            // Native Window
            var window:NativeWindow = this.stage.nativeWindow;
            window.visible = true;
            window.x = (Capabilities.screenResolutionX - window.width) / 2;
            window.y = (Capabilities.screenResolutionY - window.height) / 2;

            // Application
            var _app:App = new App();
            this.addChild(_app);

        } // End Constructor

    } // End Class
} // End Package
