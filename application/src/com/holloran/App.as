package com.holloran
{
    import com.holloran.events.FlickrSearchEvent;
    import com.holloran.views.ImageViewerView;
    import com.holloran.views.MapView;
    import com.holloran.views.SearchView;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import libs.AppBase;
    import libs.Logo;

    /**
     * <span>The App class is used as a starting point for a new application's custom chrome.</span>
     * @author dholloran
     * @example This example demonstrates how to use the App object
     * <listing version="3.0">
     *
     * var app:App = new App();
     * this.addChild( app );
     *
     * </listing>
     */
    public class App extends AppBase
    {
        private var _map:MapView;

        private var _search:SearchView;

        private var _viewer:ImageViewerView;

        private var _logo:Logo;

        public function App()
        {
            super();
            setup();
        } // End constructor


        // *************** SETUP **********************

        private function setup():void
        {
            initLogo();
            createSearch();
            createImageViewer();
            createMap();
            setupBtns();
            this.addEventListener(MouseEvent.MOUSE_DOWN, onMouseDown);
        } // End setup()

        private function initLogo():void
        {
            // Initialize Logo
            _logo = new Logo();
            _logo.x = 15;
            _logo.y = BtnClose.height + BtnClose.y;
            this.addChild(_logo);
        } // End initLogo()

        private function createSearch():void
        {
            // New Search
            _search = new SearchView();
            _search.x = _logo.x;
            _search.y = _logo.y + _logo.height + 10;
            _search.addEventListener(FlickrSearchEvent.COMPLETE, onSearchComplete)
            this.addChild(_search);
        } // End createSearch()


        private function createImageViewer():void
        {
            // New Image Viewer
            _viewer = new ImageViewerView();
            _viewer.x = _search.x;
            _viewer.y = _search.y + _search.height + 5;
            this.addChild(_viewer);

        } // End createImageViewer()

        private function createMap():void
        {
            // New Map
            _map = new MapView();
            _map.x = _viewer.width + _viewer.x + 20;
            _map.y = 20;
            this.addChild(_map);
        } // End createMap()

        private function setupBtns():void
        {
            // Close Button
            BtnClose.stop();
            BtnClose.mouseChildren = false;
            BtnClose.buttonMode = true;
            BtnClose.addEventListener(MouseEvent.ROLL_OVER, onCloseRollOver);
            BtnClose.addEventListener(MouseEvent.ROLL_OUT, onCloseRollOut);
            BtnClose.addEventListener(MouseEvent.CLICK, onCloseBtnClick);

            // Minimize Button
            BtnMini.stop();
            BtnMini.mouseChildren = false;
            BtnMini.buttonMode = true;
            BtnMini.addEventListener(MouseEvent.ROLL_OVER, onMiniRollOver);
            BtnMini.addEventListener(MouseEvent.ROLL_OUT, onMiniRollOut);
            BtnMini.addEventListener(MouseEvent.CLICK, onMiniBtnClick);
        } // End setupBtns()

        // *************** EVENTS *********************
        private function onMiniRollOver(event:MouseEvent):void
        {
            BtnMini.gotoAndStop(2);
        } // End onMiniRollOver()

        private function onMiniRollOut(event:MouseEvent):void
        {
            BtnMini.gotoAndStop(1);
        } // End onMiniRollOut()

        private function onCloseRollOver(event:MouseEvent):void
        {
            BtnClose.gotoAndStop(2);
        } // End onCloseRollOver()

        private function onCloseRollOut(event:MouseEvent):void
        {
            BtnClose.gotoAndStop(1);
        } // End onCloseRollOut()

        private function onSearchComplete(event:FlickrSearchEvent):void
        {
            _map.setPhotosInfo(event.photosInfo);
            _viewer.setPhotosInfo(event.photosInfo);
        } // End onGeoInfoComplete()

        private function onMouseDown(event:MouseEvent):void
        {
            if (event.target != _search.tfSearchInput && event.target != _viewer.tfLonOutput && event.target != _viewer.tfLatOutput)
            {
                this.stage.nativeWindow.startMove();
            }

        } // End onMouseDown()

        private function onMiniBtnClick(event:MouseEvent):void
        {
            this.stage.nativeWindow.minimize();
        } // End onMiniBtnClick() 

        private function onCloseBtnClick(event:MouseEvent):void
        {
            this.stage.nativeWindow.close();
        } // End onCloseBtnClick

    } // End Class
} // End Package
