package com.holloran.events
{
    import com.adobe.webapis.flickr.PagedPhotoList;

    import flash.events.Event;

    import mx.collections.ArrayCollection;

    /**
     * The FlickrSearchEvent event is dispatched when the SearchView class has finished searching.
     *
     * @author dholloran
     * @eventType com.holloran.events.FlickrSearchEvent.COMPLETE@eventType String
     *
     * </listing>
     */
    public class FlickrSearchEvent extends Event
    {

        /**
         * The FlickrSearchEvent.COMPLETE defines the complete state of the Flickr search.
         * @eventType complete
         */
        public static const COMPLETE:String = "complete";

        /**
         *
         * @default null
         */
        public var photosInfo:ArrayCollection;

        public function FlickrSearchEvent(type:String, bubbles:Boolean = false, cancelable:Boolean = false)
        {
            super(type, bubbles, cancelable);
        }

        public override function clone():Event
        {
            return new FlickrSearchEvent(type, bubbles, cancelable);

        }
    }
}
