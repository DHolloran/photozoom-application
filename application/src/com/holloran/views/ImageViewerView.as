package com.holloran.views
{
    import com.adobe.webapis.flickr.Photo;
    import flash.display.Bitmap;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.IOErrorEvent;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.text.TextFieldAutoSize;
    import libs.ImageViewerBase;
    import mx.collections.ArrayCollection;
    import mx.containers.Grid;

    /**
     * <span>The ImageViewerView class creates a simple photo slide show to be used with an array collection of pictures.</span>
     * @author dholloran
     * @example This example demonstrates how to use the ImageViewerView object
     * <listing version="3.0">
     *
     * var imageView:ImageViewerView = new ImageViewerView();
     * imageView.setPhotosInfo(myPhotoArray)
     * this.addChild( imageView );
     *
     * </listing>
     */

    public class ImageViewerView extends ImageViewerBase
    {

        // ******** Image/Picture Vars ********
        private var _displayPic:Bitmap;

        private var _photosInfo:ArrayCollection;

        private var _picArray:Array = [];

        private var _pic:Bitmap;

        private var _picPos:int = 0;

        private var _defaultURL:String = "libs/img/placeHolder.png";

        // ***** Location Vars ****
        private var _latitudes:Array = [];

        private var _longitudes:Array = [];

        // ******END VARS*********

        public function ImageViewerView()
        {
            super();
            setup();
        } // End constructor()

        private function setup():void
        {
            // Enables arrow key control for prev/next buttons
            this.addEventListener(KeyboardEvent.KEY_DOWN, keyDownHandler);

            // ***** BUTTONS *********
            // Previous Image Button
            btnPrevImage.stop();
            btnPrevImage = this.btnPrevImage;
            btnPrevImage.mouseChildren = false;
            btnPrevImage.buttonMode = true;
            btnPrevImage.addEventListener(MouseEvent.CLICK, onPrevClick);
            btnPrevImage.addEventListener(MouseEvent.ROLL_OVER, onBtnPrevRollOver);
            btnPrevImage.addEventListener(MouseEvent.ROLL_OUT, onBtnPrevRollOut);

            // Next Image Button
            btnNextImage.stop();
            btnNextImage.mouseChildren = false;
            btnNextImage.buttonMode = true;
            btnNextImage.addEventListener(MouseEvent.CLICK, onNextClick);
            btnNextImage.addEventListener(MouseEvent.ROLL_OVER, onBtnNextRollOver);
            btnNextImage.addEventListener(MouseEvent.ROLL_OUT, onBtnNextRollOut);

            // ***** TEXT FIELDS *********
            // Latitude value text
            tfLatOutput.autoSize = TextFieldAutoSize.LEFT;
            tfLatOutput.text = "";
            tfLatOutput.selectable = true;

            // Longitude value text
            tfLonOutput.autoSize = tfLonOutput.autoSize = TextFieldAutoSize.LEFT;
            tfLonOutput.selectable = true;
            tfLonOutput.text = ""

            //Error text	
            tfImageLoadError.selectable = false;
        } // End setup()

        private function updateTF():void
        {
            if (_latitudes && _longitudes)
            {
                // Reset Latitude & Longitude output fields 
                tfLatOutput.text = "";
                tfLonOutput.text = "";

                // Validate Longitude and Latitude information
                if (_latitudes[_picPos] == undefined)
                {
                    tfLatOutput.text = "";
                }
                else if (_longitudes[_picPos] == undefined)
                {
                    tfLonOutput.text = "";
                }
                else
                {
                    tfLatOutput.text = _latitudes[_picPos] + "";
                    tfLonOutput.text = _longitudes[_picPos] + "";
                }
            }

        } // End updateTF()

        private function nextImage():void
        {
            if (_picArray.length != 0)
            {
                if (_picPos >= _picArray.length - 1)
                {
                    // Loop back to start image
                    _picPos = 0;
                    loadImage();
                    updateTF();
                }
                else
                {
                    // Advance one image
                    _picPos++;
                    loadImage();
                    updateTF();
                }
            }
        } // End nextImage()

        private function prevImage():void
        {
            if (_picArray.length != 0)
            {
                if (_picPos <= 0)
                {
                    // Loop to last image
                    _picPos = _picArray.length - 1;
                    loadImage();
                    updateTF();
                }
                else
                {
                    // Move back one image
                    _picPos--;
                    loadImage();
                    updateTF();
                }
            }
        } // End prevImage()

        private function updateImages():void
        {
            // Setup Arrays
            _picArray = [];
            _longitudes = [];
            _latitudes = [];

            // Create loader for images
            for each (var photo:Photo in _photosInfo)
            {
                if (photo.latitude != 0 && photo.longitude != 0)
                {
                    // Location Info
                    _latitudes.push(photo.latitude);
                    _longitudes.push(photo.longitude);
                    // Sets URL for images
                    var imageURL:String = 'http://static.flickr.com/' + photo.server + '/' + photo.id + '_' + photo.secret + '_z.jpg';
                    // Image Loader
                    var request:URLRequest = new URLRequest(imageURL);
                    var imgLoader:Loader = new Loader();
                    imgLoader.load(request);
                    imgLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, onLoadComplete);
                    imgLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, imageLoadError);
                }
            }
        } // End updateImages()

        private function onLoadComplete(event:Event):void
        {
            _displayPic = event.currentTarget.content;
            _picArray.push(_displayPic);
            loadImage();
            updateTF();
        } // End onLoadComplete()

        private function loadImage():void
        {
            if (_pic)
            {
                //Clear the previous pic
                this.removeChild(_pic);
            }
            if (_picArray.length != 0)
            {
                _pic = _picArray[_picPos];

                if (_pic)
                {
                    // Set pic properties
                    _pic.width = 475;
                    _pic.height = 375;
                    _pic.x = 12.5;
                    _pic.y = 11;
                    _pic.smoothing = true;
                    this.addChild(_pic);
                }
            }
        } // End loadImage()

        // ************* EVENTS ****************
        private function onNextClick(event:MouseEvent):void
        {
            nextImage();
        } // End onNextClick

        private function onPrevClick(event:MouseEvent):void
        {
            prevImage();
        } //End on prevClick()

        private function onBtnPrevRollOut(event:MouseEvent):void
        {
            btnPrevImage.gotoAndStop(1);
        } // End onBtnPrevRollOut()

        private function onBtnPrevRollOver(event:MouseEvent):void
        {
            btnPrevImage.gotoAndStop(2);
        } // End onBtnPrevRollOver()

        private function onBtnNextRollOut(event:MouseEvent):void
        {
            btnNextImage.gotoAndStop(1);
        } // End onBtnNextRollOut()

        private function onBtnNextRollOver(event:MouseEvent):void
        {
            btnNextImage.gotoAndStop(2);
        } // End onBtnNextRollOver()

        private function keyDownHandler(event:KeyboardEvent):void
        {
            if (event.keyCode == 37)
            {
                //Left Arrow == 37  Previous Image
                prevImage();
            }
            else if (event.keyCode == 39)
            {
                //Right Arrow == 39 Next Image
                nextImage();
            }
        } // End keyDownHandler();

        // ************* ERROR HANDLER **********************
        private function imageLoadError(event:IOErrorEvent):void
        {
            this.tfImageLoadError.text = "Sorry no images please try again, thanks."
            trace("Error Handled: " + event + " " + event.errorID)
        } //End imageLoadError()

        // ************* GETTERS/SETTERS ********************
        /**
         * <span>Sets the photo array collection for use with the slideshow</span>
         * @param value
         */
        public function setPhotosInfo(photos:ArrayCollection):void
        {
            _photosInfo = photos;
            updateImages();
        } // End setPhotosInfo()
    } //End class
} //End package
