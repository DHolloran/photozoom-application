package com.holloran.views
{
    import com.adobe.webapis.flickr.FlickrService;
    import com.adobe.webapis.flickr.PagedPhotoList;
    import com.adobe.webapis.flickr.Photo;
    import com.adobe.webapis.flickr.events.FlickrResultEvent;
    import com.holloran.events.FlickrSearchEvent;
    import flash.display.Loader;
    import flash.events.Event;
    import flash.events.FocusEvent;
    import flash.events.IOErrorEvent;
    import flash.events.KeyboardEvent;
    import flash.events.MouseEvent;
    import flash.net.URLRequest;
    import flash.text.TextFieldAutoSize;
    import libs.SearchViewBase;
    import mx.collections.ArrayCollection;

    /**
     * The SearchViewBase class is used to input search queries to be searched through the Flickr search service.
     * @author dholloran
     * @example This example demonstrates how to use the SearchView object
     * <listing version="3.0">
     *
     * var searchView:SearchView = new SearchView();
     * this.addChild( searchView );
     *
     * </listing>
     */
    public class SearchView extends SearchViewBase
    {

        //***** Constants/Flickr Vars *****
        private const API_KEY:String = "09b5534922504126ca935ef795aa8c58"

        private const API_KEY_SECRET:String = "7dff1ab0932608c0"

        private var _service:FlickrService;

        //****** Photo/Images Vars *****
        private var _photos:ArrayCollection;

        //***** Search Term Vars *****
        private var _searchText:String;

        public function SearchView()
        {
            super();
            setup();
        } // End Constructor()

        private function setup():void
        {
            // Input Field Focus
            SearchBG.stop();
            tfSearchInput.addEventListener(FocusEvent.FOCUS_IN, searchFieldFocusIn);
            tfSearchInput.addEventListener(FocusEvent.FOCUS_OUT, searchFieldFocusOut);

            // Enable enter key
            this.addEventListener(KeyboardEvent.KEY_DOWN, onEnterKeyDown);

            // Textfields
            tfSearchInput.text = "";
            tfErrorText.selectable = false;


            // Submit Button
            btnSubmitSearch.stop();
            btnSubmitSearch.buttonMode = true;
            btnSubmitSearch.mouseChildren = false;
            btnSubmitSearch.addEventListener(MouseEvent.CLICK, onSubmitClick);
            btnSubmitSearch.addEventListener(MouseEvent.ROLL_OVER, onBtnSubmitRollOver);
            btnSubmitSearch.addEventListener(MouseEvent.ROLL_OUT, onBtnSubmitRollOut);

        } // End setup()

        private function searchFlickr():void
        {
            _searchText = tfSearchInput.text;
            tfSearchInput.text = "";
            // Setup Flickr search
            _service = new FlickrService(API_KEY);
            _service.photos.searchGeo("", null, null, 5, "", "any", _searchText, null, null, null, null, -1, null, 100, 1, "date-posted-desc");
            _service.addEventListener(FlickrResultEvent.PHOTOS_SEARCH, searchComplete);
            _service.addEventListener(IOErrorEvent.IO_ERROR, ifServiceError);
        } //End searchFlickr()

        private function searchComplete(event:FlickrResultEvent):void
        {

            tfErrorText.text = ""

            if (event.success)
            {
                var t:Boolean;
                var photoList:PagedPhotoList = event.data.photos;
                _photos = new ArrayCollection(photoList.photos);
                for each (var p:Photo in _photos)
                {
                    // Check if any photos are geo tagged
                    if (p.latitude != 0 && p.longitude != 0)
                    {
                        t = true;
                    }
                }
                if (t == true)
                {
                    // Dispatch custom event on successful search
                    var fse:FlickrSearchEvent = new FlickrSearchEvent("complete");
                    fse.photosInfo = _photos;
                    this.dispatchEvent(fse);
                }
                else
                {
                    tfErrorText.text = "No images exist for " + _searchText + ", please try again."
                }
            }
            else if (tfSearchInput.text == "")
            {
                tfErrorText.text = "Please enter a valid search term."
            }
        } // End searchComplete()

        // ****************** EVENTS *******************
        private function onBtnSubmitRollOver(event:MouseEvent):void
        {
            btnSubmitSearch.gotoAndStop(2);
        } // End onBtnSubmitRollOver()

        private function onBtnSubmitRollOut(event:MouseEvent):void
        {
            btnSubmitSearch.gotoAndStop(1);
        } // End onBtnSubmitRollOut()

        private function searchFieldFocusIn(event:FocusEvent):void
        {
            SearchBG.gotoAndStop(2);
        } // End searchFieldFocusIn()

        private function searchFieldFocusOut(event:FocusEvent):void
        {
            SearchBG.gotoAndStop(1);
        } //End searchFieldFocusOut()

        private function onEnterKeyDown(event:KeyboardEvent):void
        {
            //Allows use of enter key - keyCode=13
            if (event.keyCode == 13)
            {
                searchFlickr();
            }
        } // End onEnterKeyDown

        private function onSubmitClick(event:MouseEvent):void
        {
            searchFlickr();
        } // End onSubmitClick()

        // ****************** ERROR HANDLERS ****************
        private function ifServiceError(event:IOErrorEvent):void
        {
            tfErrorText.text = "";
            tfErrorText.text = "Please check your network connection"
        }
    } // End class
} // End Package
