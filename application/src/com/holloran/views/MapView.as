package com.holloran.views
{
    import com.adobe.webapis.flickr.Photo;
    import com.google.maps.LatLng;
    import com.google.maps.Map;
    import com.google.maps.MapEvent;
    import com.google.maps.MapType;
    import com.google.maps.controls.MapTypeControl;
    import com.google.maps.controls.PositionControl;
    import com.google.maps.controls.ZoomControl;
    import com.google.maps.overlays.Marker;
    import flash.events.Event;
    import flash.events.MouseEvent;
    import flash.geom.Point;
    import flash.net.URLLoader;
    import flash.net.URLRequest;
    import flash.system.System;
    import libs.MapViewBase;
    import mx.collections.ArrayCollection;

    /**
     * <span>The MapView class is used to create a Google map viewer.</span>
     * <span>As well as add markers to the map for the given longitude and latitude values of an array collection of pictures.</span>
     * @author dholloran
     * @example This example demonstrates how to use the MapView object
     * <listing version="3.0">
     *
     * var mapView:MapView = new MapView();
     * mapView.setPhotoInfo(myPhotoArray)
     * this.addChild( mapView );
     *
     * </listing>
     */
    public class MapView extends MapViewBase
    {

        // ****** Map Vars/Constants **********
        private const API_KEY:String = "ABQIAAAAft7u3ek-Axfn4VsmXE1T1BTucyl8etGgT7wsKBV1ac0-uIeWfRTRSyN49rWPMZhIvguj_GPRq-UZwQ";

        private const SITE_URL:String = "http://www.google.com";

        private var _map:Map;

        private var _markers:Array = [];

        // ***** Photo Info Vars ******
        private var _photosInfo:ArrayCollection;

        private var _photoLats:Array = [];

        private var _photoLons:Array = [];

        // ****** Location Vars *********
        private var _latitude:Number = 0;

        private var _longitude:Number = 0;

        private var _latCenter:Number = 0;

        private var _lonCenter:Number = 0;

        public function MapView()
        {
            super();
            setup();
        } //end constructor

        private function setup():void
        {
            //Create Map
            _map = new Map();
            _map.key = API_KEY;
            _map.setSize(new Point(575, 575));
            _map.x = (this.width - _map.width) / 2;
            _map.y = (this.height - _map.height) / 2;
            _map.url = "http://www.google.com";
            _map.sensor = "true";
            this.addChild(_map);
            _map.addEventListener(MapEvent.MAP_READY, onMapReady);

        } // End setup()


        private function onMapReady(e:MapEvent):void
        {
            //Set map controls
            _map.addControl(new ZoomControl());
            _map.addControl(new PositionControl());
            _map.addControl(new MapTypeControl());
            _map.buttonMode = true;

            //Set map starting location
            _map.setCenter(new LatLng(0, 0), 2, MapType.HYBRID_MAP_TYPE);

        } // End onMapReady()

        private function updateMap():void
        {
            var marker:Marker;

            // If markers exist remove markers
            if (_markers.length != 0)
            {
                for each (var m:Marker in _markers)
                {
                    _map.removeOverlay(m);
                }
            }

            // Create new markers for geo-tagged photos
            for each (var photo:Photo in _photosInfo)
            {
                if (photo.latitude != 0 && photo.longitude != 0)
                {
                    marker = new Marker(new LatLng(photo.latitude, photo.longitude));
                    _map.addOverlay(marker);
                    _photoLats.push(photo.latitude);
                    _photoLons.push(photo.longitude);
                    _markers.push(marker);
                }
            }

            //Set new map starting location
            _map.setCenter(new LatLng(_photoLats[1], _photoLons[1]), 2, MapType.HYBRID_MAP_TYPE);

        } //End updateMap()

        //************** GETTERS/SETTERS**********
        /**
         * <span>Sets the photo array collection for use with the map</span>
         * @param photos
         */
        public function setPhotosInfo(photos:ArrayCollection):void
        {
            _photosInfo = photos;
            updateMap();
        } //End setPhotoInfo()

    } // End class
} // End package
